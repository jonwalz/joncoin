import * as React from 'react'
import styled from 'styled-components'
import { GraphData } from '../types'
import { ResponsiveLine } from '@nivo/line'
import CustomTooltip from './custom_tooltip'

export interface HistoryGraphProps {
    readonly graphData: GraphData
}

const HistoryGraph: React.SFC<HistoryGraphProps> = ({ graphData }) => {
    if (graphData.data.length === 0) {
        return <div>No transactions</div>
    }
    return (
        <GraphContainerStyles>
            <ResponsiveLine
                data={[ graphData ]}
                margin={{ top: 50, right: 200, bottom: 50, left: 60 }}
                lineWidth={2}
                pointSize={8}
                crosshairType="cross"
                animate={false}
                xScale={{
                    type: 'time',
                    format: '%Y-%m-%dT%H:%M:%S.%LZ',
                    precision: 'minute',
                }}
                xFormat="time:%Y-%m-%d %H:%M:%S"
                axisBottom={{
                    tickPadding: 5,
                    legend: 'Transactions',
                    legendOffset: 35,
                    legendPosition: 'middle',
                    format: '%b %d %M',
                    tickValues: 'every day',
                }}
                axisLeft={{
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: 'Coins',
                    legendOffset: -40,
                    legendPosition: 'middle',
                }}
                useMesh={true}
                enableSlices="y"
                sliceTooltip={(data) => <CustomTooltip data={data}/>}
            />
        </GraphContainerStyles>
    )
}

export default HistoryGraph

const GraphContainerStyles = styled.div`
    grid-row-start: 2;
    grid-row-end: 4;
    display: flex;
    flex-direction: column;
    flex: 1;
    border: 2px solid gray;
    border-radius: 5px;
    align-items: center;
`
