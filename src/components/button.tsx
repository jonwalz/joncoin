import * as React from 'react'
import styled from 'styled-components'

export interface ButtonProps {
    readonly onClick?: (
        e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    ) => void
    readonly type: 'button' | 'submit' | 'reset' | undefined
}

const Button: React.SFC<ButtonProps> = (props) => {
    const { type = 'submit' } = props
    return (
        <StyledButton type={type} onClick={props.onClick}>
            {props.children}
        </StyledButton>
    )
}

export default Button

const StyledButton = styled.button`
    color: white;
    background-color: slategray;
    margin-top: 10px;
    height: 27px;
    border: 2px solid gray;
    border-radius: 5px;
    &:focus {
        outline: none;
    }
`
