import * as React from 'react'
import styled from 'styled-components'
import Button from './button'
import { TransactionFormData } from '../types'

export interface SendCoinFormProps {
    readonly postTransaction: (transactionFormData: TransactionFormData) => void
    readonly currentUserAddress: string
}

const SendCoinForm: React.SFC<SendCoinFormProps> = (props) => {
    const { postTransaction, currentUserAddress } = props
    const [ toAddress, setToAddress ] = React.useState('')
    const [ amount, setAmount ] = React.useState('')

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault()
        postTransaction({
            fromAddress: currentUserAddress,
            toAddress,
            amount,
        })
        setAmount('')
        setToAddress('')
    }

    return (
        <StyledSendCoinFormContainer>
            <h2>SendCoinForm</h2>
            <div className="divider" />
            <form onSubmit={handleSubmit}>
                <label htmlFor="destination-address">Destination Address</label>
                <input
                    value={toAddress}
                    id="destination-address"
                    onChange={(e) => setToAddress(e.target.value)}
                />

                <label htmlFor="amount-input">Amount to send</label>
                <input
                    value={amount}
                    id="amount-input"
                    onChange={(e) => setAmount(e.target.value)}
                />
                <Button type="submit">Send Jobcoin</Button>
            </form>
        </StyledSendCoinFormContainer>
    )
}

export default SendCoinForm

const StyledSendCoinFormContainer = styled.div`
    grid-row-start: 3;
    display: flex;
    flex-direction: column;
    border: 2px solid gray;
    border-radius: 5px;
    align-items: center;

    form {
        display: flex;
        flex-direction: column;
        padding: 20px;
        width: 100%;
    }

    label,
    input {
        margin-bottom: 7px;
    }

    input {
        display: flex;
        padding: 5px;
        border: 2px solid gray;
        border-radius: 5px;
        user-select: none;

        &:focus {
            outline: none;
        }
    }
    .divider {
        border-bottom: 2px solid gray;
        width: 100%;
    }
`
