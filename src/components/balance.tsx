import * as React from 'react'
import styled from 'styled-components'

export interface BalanceProps {
    readonly currentBalance: string
}

const Balance: React.SFC<BalanceProps> = (props) => {
    const { currentBalance = 0 } = props
    return (
        <StyledBalanceContainer>
            <h2>Jobcoin Balance</h2>
            <div className="divider" />
            <h2>{currentBalance}</h2>
        </StyledBalanceContainer>
    )
}

export default Balance

const StyledBalanceContainer = styled.div`
    grid-row-start: 2;
    display: flex;
    flex-direction: column;
    flex: 1;
    border: 2px solid gray;
    border-radius: 5px;
    align-items: center;

    h2 {
        display: flex;
        flex-direction: column;
        flex: 1;
        justify-content: center;
    }

    .divider {
        border-bottom: 2px solid gray;
        width: 100%;
    }
`
