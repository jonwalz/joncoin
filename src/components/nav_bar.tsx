import * as React from 'react'
import styled from 'styled-components'

export interface NavBarProps {
    readonly currentUserAddress: string
    readonly handleSignOut: () => void
}

const NavBar: React.SFC<NavBarProps> = ({ currentUserAddress, handleSignOut }) => {
    return (
        <NavbarStyles>
            <div className="jobcoin-sender">
                <div className="fa fa-thumbs-up" aria-hidden="true" />
                <span>Jobcoin Sender</span>
            </div>
            <div className="options">
                <div className="fa fa-user-circle" aria-hidden="true" />
                <span>{currentUserAddress}</span>
                <button onClick={handleSignOut}>Sign Out</button>
            </div>
        </NavbarStyles>
    )
}

export default NavBar

const NavbarStyles = styled.div`
    grid-column-start: 1;
    grid-column-end: 3;
    grid-row-start: 1;
    display: flex;
    justify-content: space-between;
    padding-bottom: 5px;

    .jobcoin-sender {
        display: flex;
        align-items: center;
        span {
            margin: 0 5px;
        }
    }

    .options {
        display: flex;
        align-items: center;
        span, a {
            display: inline-flex;
            margin: 0 5px;
        }
    }

    button {
        border: none;
        color: slategray;
        cursor: pointer;
    }
`
