import * as React from 'react'
import styled from 'styled-components'

export interface CustomTooltipProps {
    readonly data: any
}

const CustomTooltip: React.SFC<CustomTooltipProps> = (props) => {
    const { slice: { points } } = props.data
    const {
        data: {
            y: total,
            transaction: { type, amount, destination, dateString },
        },
    } = points[0]
    return (
        <StyledToolTip style={{ padding: '9px' }}>
            <h3>{total}</h3>
            <div>
                {type} of {amount}
            </div>
            <div>{destination}</div>
            <div>{dateString}</div>
        </StyledToolTip>
    )
}

export default CustomTooltip

const StyledToolTip = styled.div`
    background-color: white;
    border: 2px solid slategray;
    border-radius: 5px;
    h3 {
        margin: 0;
    }
`
