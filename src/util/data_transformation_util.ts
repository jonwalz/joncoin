import { RemoteTransaction, TransactionDataPoint } from '../types'
import moment from 'moment'

// TODO: Refactor and simplify
const transformRemoteData = (
    remoteTransactions: ReadonlyArray<RemoteTransaction>,
    currentUserAddress: string,
): TransactionDataPoint[] => {
    let total: number
    return remoteTransactions.reduce(
        (
            acc: TransactionDataPoint[],
            transaction: RemoteTransaction,
            index,
        ) => {
            const readableTime = convertToGraphableTime(transaction.timestamp)

            // The first item represents the starting total
            if (index === 0) {
                total = parseFloat(transaction.amount)
                return [
                    {
                        x: transaction.timestamp,
                        y: transaction.amount.toString(),
                        transaction: {
                            type: 'Initial Deposit',
                            destination: null,
                            dateString: readableTime,
                            amount: transaction.amount,
                        },
                    },
                ]
            }

            const computedTotal = computeTotalFromUser(
                transaction,
                currentUserAddress,
                total,
            )
            const transactionType =
                computedTotal < total ? 'Withdrawl' : 'Deposit'
            const destinationType =
                transactionType === 'Withdrawl'
                    ? transaction.toAddress
                    : transaction.fromAddress

            total = computedTotal
            return [
                ...acc,
                {
                    x: transaction.timestamp,
                    y: computedTotal.toString(),
                    transaction: {
                        type: transactionType,
                        destination: destinationType,
                        dateString: readableTime,
                        amount: transaction.amount,
                    },
                },
            ]
        },
        [],
    )
}

/* Creates a human readable timestamp for the graph */
const convertToGraphableTime = (timestamp: string) => {
    return moment(timestamp).format('lll')
}

const computeTotalFromUser = (
    transaction: RemoteTransaction,
    currentUserAddress: string,
    total: number,
) => {
    // If toAddress matches the current user, then receive coins
    // Else lose coins :(
    return transaction.toAddress === currentUserAddress
        ? total + parseFloat(transaction.amount)
        : total - parseFloat(transaction.amount)
}

export { transformRemoteData, convertToGraphableTime, computeTotalFromUser }
