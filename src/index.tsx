import * as React from 'react'
import { render } from 'react-dom'
import styled from 'styled-components'
import { AppState, TransactionFormData } from './types'
import axios, { AxiosResponse, AxiosError } from 'axios'
import Login from './scenes/login'
import Dashboard from './scenes/dashboard'
import { transformRemoteData } from './util/data_transformation_util'
require('./styles.css')
require('../node_modules/@blueprintjs/core/lib/css/blueprint.css')
const url = 'https://jobcoin.gemini.com/thesaurus/api'

const StyledApp = styled.div`
    height: 100vh;
    display: flex;
`

class App extends React.Component<{}, AppState> {
    constructor(props: {}) {
        super(props)

        // Default app state
        this.state = {
            currentUserAddress: '',
            currentBalance: '',
            graphData: {
                id: '',
                data: [],
            },
            error: {
                hasError: false,
                errorMessage: '',
            },
            transactionFormData: {
                fromAddress: '',
                toAddress: '',
                amount: '',
            },
        }

        this.getUserData = this.getUserData.bind(this)
        this.postTransaction = this.postTransaction.bind(this)
        this.handleSignOut = this.handleSignOut.bind(this)
    }

    componentWillMount(): void {
        // If user has logged in, username should be in local storage
        const localUser = localStorage.getItem('currentUserAddress')
        if (localUser && localUser !== '') {
            this.setState({
                currentUserAddress: localUser,
            })

            this.getUserData(localUser)
        }
    }

    getUserData(currentUserAddress: string): void {
        // TODO: Refactor client calls to separate file
        axios
            .get(`${url}/addresses/${currentUserAddress}`)
            .then((response: AxiosResponse) => {
                // handle success
                const currentBalance = response.data.balance
                const transformedData = transformRemoteData(
                    response.data.transactions,
                    currentUserAddress,
                )
                const graphData = {
                    id: currentUserAddress,
                    data: transformedData,
                }

                this.setState(
                    {
                        currentUserAddress,
                        currentBalance,
                        graphData,
                    },
                    () =>
                        // set user in local storage to persist login on refresh
                        localStorage.setItem(
                            'currentUserAddress',
                            this.state.currentUserAddress,
                        ),
                )
            })
            .catch((error) => {
                this.setState({
                    error: {
                        hasError: true,
                        errorMessage: error.message,
                    },
                })
            })
    }

    postTransaction(transactionFormData: TransactionFormData): void {
        axios
            .post(`${url}/transactions`, transactionFormData)
            .then((response: AxiosResponse) => {
                // handle success
                this.getUserData(this.state.currentUserAddress)
            })
            .catch((err: AxiosError) => {
                // handle error
                this.setState({
                    error: {
                        hasError: true,
                        errorMessage: err.message,
                    },
                })
            })
    }

    handleSignOut() {
        this.setState({
            currentUserAddress: '',
        })
        localStorage.removeItem('currentUserAddress')
    }

    render() {
        const { currentUserAddress, currentBalance, graphData } = this.state
        const isLoggedIn = currentUserAddress !== ''
        return (
            <StyledApp>
                {isLoggedIn ? (
                    <Dashboard
                        currentBalance={currentBalance}
                        postTransaction={this.postTransaction}
                        getUserData={this.getUserData}
                        currentUserAddress={currentUserAddress}
                        graphData={graphData}
                        handleSignOut={this.handleSignOut}
                    />
                ) : (
                    <Login getUserData={this.getUserData} />
                )}
            </StyledApp>
        )
    }
}

const rootElement = document.getElementById('root')
render(<App />, rootElement)
