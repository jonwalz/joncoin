export interface AppState {
	readonly currentUserAddress: string
	readonly currentBalance: string
	readonly graphData: GraphData
	readonly transactionFormData: TransactionFormData
	readonly error: ErrorState
}

export interface GraphData {
	readonly id: string
	readonly data: TransactionDataPoint[]
}

export interface TransactionDataPoint {
	readonly x: Time
	readonly y: Balance
}

type Time = string
type Balance = string

export interface ErrorState {
	readonly hasError: boolean
	readonly errorMessage: string
}

export interface TransactionFormData {
	readonly fromAddress: string
	readonly toAddress: string
	readonly amount: string
}

/*******************
    Remote types
********************/

export interface RemoteAddressInfo {
    readonly balance: string
    readonly transactions: ReadonlyArray<RemoteTransaction>
}
export interface RemoteTransaction {
	readonly timestamp: string
	readonly toAddress: string
	readonly fromAddress: string
	readonly amount: string
}