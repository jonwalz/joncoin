import { RemoteTransaction } from './types'

const remoteGraphData = [
    {
        timestamp: '2019-06-07T03:27:23.199Z',
        toAddress: 'Bella',
        amount: '50',
    },
    {
        timestamp: '2019-06-09T05:34:15.106Z',
        fromAddress: 'Bella',
        toAddress: 'Morgan',
        amount: '2',
    },
    {
        timestamp: '2019-06-09T05:34:51.973Z',
        fromAddress: 'Bella',
        toAddress: 'Morgan',
        amount: '2',
    },
] as ReadonlyArray<RemoteTransaction>

const computeTotalFromUserTestData = {
    transaction: {
        toAddress: 'Bella',
        fromAddress: 'Morgan',
        amount: '10',
    } as RemoteTransaction,
    currentUserAddress: 'Bella',
    total: 10,
}

export { remoteGraphData, computeTotalFromUserTestData }
