
import styled from "styled-components"

export const LoginContainer = styled.div`
    margin: 100px auto;

    .login-card {
        display: flex;
        flex-direction: column;
        border: 2px solid gray;
        border-radius: 5px;
        width: 300px;
        min-height: 300px;
        justify-content: space-around;
    }

    .welcome-message {
        padding: 20px;
        text-align: center;
    }

    .input-form {
        padding: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        width: 100%;
    }

    .login-label {
        margin-bottom: 7px;
    }

    .login-input {
        display: flex;
        padding: 5px;
        border: 2px solid gray;
        border-radius: 5px;
        user-select: none;
        &:focus {
            outline: none;
        }
    }

    .logo {
        margin-bottom: 100px;
        text-align: center;
    }

    .divider {
        border-bottom: 2px solid gray;
    }
`
