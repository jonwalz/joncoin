import * as React from 'react'
import { LoginContainer } from './login_styles'
import Button from '../components/button'

export interface LoginProps {
    readonly getUserData: (userAddress: string) => void
}

const Login: React.SFC<LoginProps> = (props) => {
    const [ inputState, setInputState ] = React.useState('')
    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault()
        props.getUserData(inputState)
    }

    return (
        <LoginContainer>
            <div className="logo">
                <div className="fa fa-thumbs-up fa-5x" aria-hidden="true" />
            </div>
            <div className="login-card">
                <div className="welcome-message">
                    <h2>Welcome! Sign In With Your Jobcoin Address</h2>
                </div>
                <div className="divider" />
                <form className="input-form" onSubmit={handleSubmit}>
                    <label htmlFor="login-field" className="login-label">
                        Jobcoin Address
                    </label>
                    <input
                        autoFocus={true}
                        id="login-field"
                        className="login-input"
                        value={inputState}
                        onChange={(e) => setInputState(e.target.value)}
                    />
                    <Button type="submit">Sign In</Button>
                </form>
            </div>
        </LoginContainer>
    )
}

export default Login
