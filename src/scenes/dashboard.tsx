import * as React from 'react'
import styled from 'styled-components'
import Balance from '../components/balance'
import SendCoinForm from '../components/send_coin_form'
import HistoryGraph from '../components/history_graph'
import NavBar from '../components/nav_bar'
import { TransactionFormData, GraphData } from '../types'

export interface DashboardProps {
    readonly currentBalance: string
    readonly currentUserAddress: string
    readonly graphData: GraphData
    readonly postTransaction: (transactionFormData: TransactionFormData) => void
    readonly getUserData: (id: string) => void
    readonly handleSignOut: () => void
}

const Dashboard: React.SFC<DashboardProps> = (props) => {
    const {
        currentBalance,
        postTransaction,
        currentUserAddress,
        graphData,
        handleSignOut,
    } = props
    return (
        <StyledDashboard>
            <NavBar
                currentUserAddress={currentUserAddress}
                handleSignOut={handleSignOut}
            />
            <Balance currentBalance={currentBalance} />
            <SendCoinForm
                currentUserAddress={currentUserAddress}
                postTransaction={postTransaction}
            />
            <HistoryGraph graphData={graphData} />
        </StyledDashboard>
    )
}

export default Dashboard

const StyledDashboard = styled.div`
    display: grid;
    grid-template-columns: 300px auto;
    grid-template-rows: auto 1fr 3fr;
    grid-gap: 10px;
    flex: 1;
    padding: 20px;
`
