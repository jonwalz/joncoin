import { remoteGraphData, computeTotalFromUserTestData } from '../test_data'
import {
    transformRemoteData,
    convertToGraphableTime,
    computeTotalFromUser,
} from '../util/data_transformation_util'

describe('Confirm transformRemoteData', () => {
    // Happy path
    test('Returns correct shape if given correct data', () => {
        const actual = transformRemoteData(remoteGraphData, 'Bella')
        const expected = [
            {
                x: '2019-06-07T03:27:23.199Z',
                y: '50',
                transaction: {
                    destination: null,
                    type: 'Initial Deposit',
                    dateString: 'Jun 6, 2019 8:27 PM',
                    amount: "50",
                },
            },
            {
                x: '2019-06-09T05:34:15.106Z',
                y: '48',
                transaction: {
                    destination: 'Morgan',
                    type: 'Withdrawl',
                    dateString: 'Jun 8, 2019 10:34 PM',
                    amount: "2",
                },
            },
            {
                x: '2019-06-09T05:34:51.973Z',
                y: '46',
                transaction: {
                    destination: 'Morgan',
                    type: 'Withdrawl',
                    dateString: 'Jun 8, 2019 10:34 PM',
                    amount: "2"
                },
            },
        ]
        expect(actual).toEqual(expected)
    })
})

describe('Confirm convertToGraphableTime', () => {
    test('Should return accurate format of date time string', () => {
        const timeStamp = '2019-06-07T03:27:23.199Z'
        const actual = convertToGraphableTime(timeStamp)
        const expected = "Jun 6, 2019 8:27 PM"

        expect(actual).toEqual(expected)
    })
})

describe('Confrim computeTotalFromUser', () => {
    test('Should add to total if toAddress matches currentUseraddress', () => {
        const {
            transaction,
            currentUserAddress,
            total,
        } = computeTotalFromUserTestData
        const actual = computeTotalFromUser(
            transaction,
            currentUserAddress,
            total,
        )
        const expected = 20
        expect(actual).toEqual(expected)
    })

    test('Should subtract from total if toAddress does not match currentUserAddress', () => {
        const { transaction, total } = computeTotalFromUserTestData
        const actual = computeTotalFromUser(
            transaction,
            'NotCurrentUser',
            total,
        )
        const expected = 0

        expect(actual).toEqual(expected)
    })
})
