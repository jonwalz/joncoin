# Setup

I had a lot of fun making this! Thank you for the chance to test my skills.

You can find a deployed working version here: [https://joncoin-vxtnzpoztg.now.sh](https://joncoin-vxtnzpoztg.now.sh)

A couple users (or "addresses") I have been using are `Bella` and `Morgan`.

My process stared with a technical design that you will find below. Notes on retrospective are at the bottom.

To run the app locally:

    yarn # install dependencies
    yarn start # run the development server
    yarn test # to run the tests

I likely spent a little too much time on the technical design and polishing the style of the app (but I had the whole weekend and was enjoying myself)

According to my time tracker, the technical design took roughly 2.5 hours and coding took 8 hours. Total time could have been reduced by implementing code while writing technical design.

---

# Technical Design

This document is designed to give a general path of execution to complete the application requirements for Job Coin. I've found creating technical design helps identify
potential expensive tasks and needed research without getting distracted by code and debugging.

# High level design

### Chosen Technologies

- [React](https://reactjs.org) - For UI development
- [Create-react-app](https://github.com/facebook/create-react-app) - To quickly bootstrap the application
- [Typescript](https://www.typescriptlang.org/) - for type safety
- [Axios](https://github.com/axios/axios) - for data fetching
- [Nivo](https://nivo.rocks/components) - @nivo/line to easily render a nice looking line chart and not worry about styling
- [Styled-components](https://www.styled-components.com/) - for customizing styles of components
- [Jest](https://github.com/facebook/jest) - for unit testing
- [react-testing-library](https://github.com/testing-library/react-testing-library) - For testing react components (Haven't actually used this, may be a stretch goal)
- [Blueprint.js](https://blueprintjs.com/) - A familiar UI library of react components to quickly and easily make a nice looking UI
- Stretch goal: Add user icon library
- Stretch goal: Deploy using [Now by Zeit](https://zeit.co/now)
- Stretch goal: Add tests to deployment pipeline

Notes: 

I considered redux and redux-saga in this technical design but decided the scope of this application too small to need them. These libraries do help react applications scale. If the requirements indicated the app would need to scale then I would have included them.

I considered react-router but again concluded it to be out of scope. React router would be a good demonstration for navigating from login to the dashboard via a protected route. This may become a stretch goal if time permits.

I chose client side rendering because the application is a representation of my front-end abilities ;) 

### High Level TODOs:

- [X]  Bootstrap development cycle using create-react-app
- [X]  Install necessary dependencies
- [X]  Define tests, components, client, and utility function architecture

# Low Level Design

### Setup

Use create-react-app with typescript to quickly initialize the project:

    npx create-react-app joncoin --typescript 

Setup git remote:

(create react app initializing a git repo for you)

    git remote add origin git@github.com:jonwalz/JonCoin.git
    git push -u origin master

Add dependencies:

    yarn add axios @nivo/line @blueprintjs/core styled-components jest

### Setup blueprint css

Blueprint needs a custom stylesheet for their component styles

In app.tsx

    require("../node_modules/@blueprintjs/core/lib/css/blueprint.css")

### Directory architecture

Create a `scenes` directory with two container components:

    cd src
    mkdir scenes && cd scenes
    touch login.tsx dashboard.tsx 

Create a `components` directory with the following components:

    mkdir components && cd components
    touch balance.tsx send_coin_form.tsx history_graph.tsx navbar.tsx

Create a `tests` directory and add test files for components and utility functions

    cd src
    mkdir tests and cd tests
    touch components.tests.ts utility.tests.ts

Create a `util` directory to handle applications utilities

    cd src
    mkdir util
    touch data_transformation_util.ts

### Data strucutres

We need to consider the shape of data for the UI and where to handle state changes.

Core data structure for app:

    interface AppState {
    	readonly currentUserAddress: string
    	readonly currentBalance: string
    	readonly graphData: GraphData
    	readonly formData: FormData
    	readonly error: ErrorState
    }
    
    interface GraphData {
    	readonly id: string
    	readonly data: ReadonlyArray<TransactionDataPoint>
    }
    
    interface TransactionDataPoint {
    	readonly x: Time
    	readonly y: Balance
    }
    
    type Time = string
    type Balance = string
    
    interface RemoteTransaction {
    	readonly timeStamp: string
    	readonly toAddress: string
    	readonly fromAddress: string
    	readonly amount: string
    }
    
    interface ErrorState {
    	readonly hasError: boolean
    	readonly errorMessage: string
    }
    
    interface FormData {
    	readonly fromAddress: string
    	readonly toAddress: string
    	readonly amount: string
    ]

### Data Transformation utilities

Incoming transaction data will need to be transformed into a usable shape for the graph

    // data_transformation_util.ts
    // ReadonlyArray<RemoteTransaction> -> GraphData
    function transformRemoteTransaction(): GraphData {
    ...
    }

Write test for transformation function

    describe("Confirm transformRemoteTransaction", () => {
    	test("Should return array of TransactionDataPoint", () => {})
    	test("Should return empty array if error", () => {})
    })

### User input util

form inputs will need to be sanitized and validated

    // form_utils.ts
    
    function sanitizeDestinationInput(): string {
    }
    
    function sanitizeAmountInput(input: string): string {
    }
    
    function inputDebound(): string {
    }

Write tests for sanitizing utils

    describe("Confirm sanitizeDestinationInput", () => {
    	test("Should remove white space", () => {})
    	test("Should only contain letters and numbers", () => {})
    })

### Setup App.tsx

App will handle global state

> Usually I would redux for state management but this apps global state is simple enough to avoid the bloat and boiler plate of redux.

    class App extends React.Component<{}, AppState> {
    	constructor(props: {})
    		super(props)
    		
    		// default state
    		this.state = {
    			currentUserAddress: "",
    			currentBalance: "",
    			graphData: "",
    	}

    }

Set up client calls in App component

> In an application scoped to scale, I would separate the client logic into it's own file/directory. This application will only have two calls and can easily be setup as methods in the App component. I recognize this is not ideal because client calls are concerns of the dashboard component and not app component but it makes the api easy to work with.

    // App.tsx
    ...
    
    componentDidMount(): void {
    	this.getUserData()
    }
    
    getUserData(): void {
    	const { currentUserAddress } = this.state
    	const url = "" // This could be stored in env variable for privacy
    
    	axios.get(`${url}/${currentUserAddress}`)
    		.then((response) => {
    			// handle success
    		})
    		.catch(() => {
    			// handle error
    		})
    		.finally(() => {
    			// a good place to stop a loading spinner
    		})
    }
    
    postTransaction(formData: FormData): void {
    	axios.post(`${url}/`, formData})
    		... // similar to above
    }

### Setup Login.tsx

Login will be child of app

Will need to submit a user string and navigate to dashboard on success

### Setup Dashboard.tsx

Dashboard will be a container for styles and delegating method calls passed through from the app component.

    // Dashboard.tsx
    
    interface DashboardProps {
    	readonly postTransaction: (formData: FormData) => void
    	readonly currentUserAddress: string
    	readonly currentBalance: string
    	readonly graphData: GraphData
    	readonly error: ErrorState
    }
    
    // Will be parent component to 
    	// Balance
    	// Send form
    	// Graph

### navbar.tsx component

- Will need to display user address and placeholder gravitar
- Will need logout button → should return to login screen when clicked
- Will need to indicate logged in

### send_coin_form.tsx component

- Will need to manage state for user input
- Will need to call submit function passing correct data

### history_graph.tsx component

- Will display graph of current balance over time
- Would be nice to display transaction details on hover of a specific transaction

### Stretch goal: spinner.tsx

- shown when asynchronous things are happening

---

# Retrospective

Typescript setup was more involved than expected.

Didn't need to install jest since it was included with create-react-app.

Should handle the temporary redirect rather than just pulling out and using the url. I'm guessing this was part of the test ;)

Added Moment.js for time conversion.

Could use context to avoid passing props through.

Decided to persist user with local storage to maintain "logged in" on refresh.

Could spend more time setting up test environment and adding more tests. Snapshot tests?

Should it scroll horizontally?

Could use some performance evaluation and refactoring.